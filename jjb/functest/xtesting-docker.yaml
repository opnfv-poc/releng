---
##############################################
# job configuration for docker build and push
##############################################
- project:

    name: xtesting-docker

    project: functest-xtesting

    stream:
      - master:
          branch: '{stream}'
          disabled: false
      - kali:
          branch: 'stable/{stream}'
          disabled: false
      - jerma:
          branch: 'stable/{stream}'
          disabled: false
      - iruya:
          branch: 'stable/{stream}'
          disabled: false
      - hunter:
          branch: 'stable/{stream}'
          disabled: false

    arch_tag:
      - 'amd64':
          slave_label: 'lf-build2'
      - 'arm64':
          slave_label: 'opnfv-build-ubuntu-arm'

    image:
      - 'core'
      - 'mts'

    exclude:
      - stream: 'kali'
        image: 'mts'
      - stream: 'jerma'
        image: 'mts'
      - stream: 'iruya'
        image: 'mts'
      - stream: 'hunter'
        image: 'mts'

    # settings for jobs run in multijob phases
    build-job-settings: &build-job-settings
      current-parameters: false
      git-revision: true
      node-parameters: false
      predefined-parameters: |
        PUSH_IMAGE=$PUSH_IMAGE
        COMMIT_ID=$COMMIT_ID
        GERRIT_REFNAME=$GERRIT_REFNAME
        DOCKERFILE=$DOCKERFILE
      kill-phase-on: FAILURE
      abort-all-jobs: false

    manifest-job-settings: &manifest-job-settings
      current-parameters: false
      git-revision: true
      node-parameters: false
      predefined-parameters:
        GERRIT_REFNAME=$GERRIT_REFNAME
      kill-phase-on: FAILURE
      abort-all-jobs: false

    # yamllint enable rule:key-duplicates
    jobs:
      - "xtesting-docker-{stream}"
      - "xtesting-{image}-docker-build-{arch_tag}-{stream}"
      - "xtesting-{image}-docker-manifest-{stream}"

########################
# job templates
########################
- job-template:
    name: 'xtesting-docker-{stream}'

    project-type: multijob

    disabled: '{obj:disabled}'

    parameters:
      - xtesting-job-parameters:
          project: '{project}'
          branch: '{branch}'
          slave_label: 'lf-build2'
          arch_tag: 'amd64'

    properties:
      - throttle:
          max-per-node: 1
          option: 'project'

    scm:
      - git-scm

    triggers:
      - pollscm:
          cron: "*/30 * * * *"
      - gerrit-trigger-tag-created:
          project: '{project}'

    builders:
      - multijob:
          name: 'build xtesting images'
          execution-type: PARALLEL
          projects:
            - name: 'xtesting-core-docker-build-amd64-{stream}'
              <<: *build-job-settings
            - name: 'xtesting-core-docker-build-arm64-{stream}'
              <<: *build-job-settings
      - multijob:
          name: 'publish xtesting manifests'
          execution-type: PARALLEL
          projects:
            - name: 'xtesting-core-docker-manifest-{stream}'
              <<: *manifest-job-settings
      - multijob:
          name: 'build xtesting-mts images'
          execution-type: PARALLEL
          projects:
            - name: 'xtesting-mts-docker-build-amd64-{stream}'
              <<: *build-job-settings
            - name: 'xtesting-mts-docker-build-arm64-{stream}'
              <<: *build-job-settings
      - multijob:
          name: 'publish xtesting-mts manifests'
          execution-type: PARALLEL
          projects:
            - name: 'xtesting-mts-docker-manifest-{stream}'
              <<: *manifest-job-settings


    publishers:
      - 'xtesting-amd64-recipients'
      - 'xtesting-arm64-recipients'

- job-template:
    name: 'xtesting-{image}-docker-build-{arch_tag}-{stream}'
    disabled: '{obj:disabled}'
    parameters:
      - xtesting-job-parameters:
          project: '{project}'
          branch: '{branch}'
          slave_label: '{slave_label}'
          arch_tag: '{arch_tag}'
    scm:
      - git-scm
    builders:
      - shell: |
          #!/bin/bash -ex
          case "{arch_tag}" in
          "arm64")
              sudo arch=arm64 amd64_dirs= arm64_dirs=docker/{image} arm_dirs= bash ./build.sh ;;
          *)
              sudo arch=amd64 amd64_dirs=docker/{image} arm64_dirs= arm_dirs= bash ./build.sh ;;
          esac
          exit $?

- job-template:
    name: 'xtesting-{image}-docker-manifest-{stream}'

    parameters:
      - project-parameter:
          project: '{project}'
          branch: '{branch}'
      - label:
          name: SLAVE_LABEL
          default: 'lf-build2'
          description: 'Slave label on Jenkins'
          all-nodes: false
          node-eligibility: 'ignore-offline'
      - string:
          name: PROJECT
          default: "{project}"
          description: "Project name used to enable job conditions"
      - string:
          name: GIT_BASE
          default: https://gerrit.opnfv.org/gerrit/$PROJECT
          description: 'Git URL to use on this Jenkins Slave'
      - string:
          name: REPO
          default: "opnfv"
          description: "Repository name for xtesting images"


    disabled: '{obj:disabled}'

    builders:
      - shell: |
          #!/bin/bash -ex
          case "{stream}" in
          "master")
              tag="latest" ;;
          *)
              tag="{stream}" ;;
          esac
          case "{image}" in
          "core")
              img="" ;;
          *)
              img="-{image}" ;;
          esac
          sudo manifest-tool push from-args \
              --platforms linux/amd64,linux/arm64 \
              --template $REPO/xtesting$img:ARCH-$tag \
              --target $REPO/xtesting$img:$tag
          exit $?

- parameter:
    name: xtesting-job-parameters
    parameters:
      - project-parameter:
          project: '{project}'
          branch: '{branch}'
      - label:
          name: SLAVE_LABEL
          default: '{slave_label}'
          description: 'Slave label on Jenkins'
          all-nodes: false
          node-eligibility: 'ignore-offline'
      - string:
          name: GIT_BASE
          default: https://gerrit.opnfv.org/gerrit/$PROJECT
          description: 'Git URL to use on this Jenkins Slave'
      - string:
          name: PUSH_IMAGE
          default: "true"
          description: "To enable/disable pushing the image to Dockerhub."
      - string:
          name: COMMIT_ID
          default: ""
          description: "commit id to make a snapshot docker image"
      - string:
          name: GERRIT_REFNAME
          default: ""
          description: "Docker tag to be built, e.g. refs/tags/5.0.0, refs/tags/opnfv-5.0.0, refs/tags/5.0.RC1"
      - string:
          name: DOCKERFILE
          default: "Dockerfile"
          description: "Dockerfile to use for creating the image."
      - string:
          name: ARCH_TAG
          default: "{arch_tag}"
          description: "If set, this value will be added to the docker image tag as a prefix"
      - string:
          name: PROJECT
          default: "{project}"
          description: "Project name used to enable job conditions"
      - string:
          name: REPO
          default: "opnfv"
          description: "Repository name for xtesting images"

# publisher macros
- publisher:
    name: 'xtesting-arm64-recipients'
    publishers:
      - email:
          recipients: >
            cristina.pauna@enea.com
            alexandru.avadanii@enea.com
            delia.popescu@enea.com

- publisher:
    name: 'xtesting-amd64-recipients'
    publishers:
      - email:
          recipients: >
            jalausuch@suse.com morgan.richomme@orange.com
            cedric.ollivier@orange.com feng.xiaowei@zte.com.cn
            juha.kosonen@nokia.com wangwulin@huawei.com
            valentin.boucher@kontron.com
